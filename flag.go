package delivery

import (
	"fmt"
)

func setPacketIDPresent(flag byte) byte {
	return flag | 128
}

func setAscIDPresent(flag byte) byte {
	return flag | 64
}

func isPacketIDPresent(flag byte) bool {
	return (flag&128)>>7 == 1
}

func isAscIDPresent(flag byte) bool {
	return (flag&64)>>6 == 1
}

func getSequenceFlag(flag byte) bool {
	return (flag&32)>>5 == 1
}

func setSequenceFlag(flag byte, sequenceFlag bool) byte {
	if sequenceFlag {
		return flag | 32
	}
	return flag | 0
}

func getAscIDCount(flag byte) byte {
	return flag & 31
}

func setAscIDCount(flag byte, count byte) byte {
	return flag + count
}

func sprintFlag(flag byte) string {
	return fmt.Sprintf("PacketIDPresent: %t, AscIDPresent: %t, AscIDCount: %d", isPacketIDPresent(flag), isAscIDPresent(flag), getAscIDCount(flag))
}
