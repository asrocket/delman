package delivery

import "testing"

func TestNewManagerError(t *testing.T) {
	err := NewManagerError(DataLengthError, "Msg")

	if err.code != DataLengthError || err.msg != "Msg" || err.Error() != "Msg" {
		t.Fail()
	}
}
