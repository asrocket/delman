package delivery

import (
	"math"
	"testing"
)

func TestSetPacketIDPresent(t *testing.T) {
	var i byte
	for ; i < 128; i++ {
		f := setPacketIDPresent(i)
		if f != 128+i {
			t.Fail()
		}
	}
}

func TestSetAscIDPresent(t *testing.T) {
	var i byte
	for ; i < 64; i++ {
		f := setAscIDPresent(i)
		if f != 64+i {
			t.Fail()
		}
	}
}

func TestIsPacketIDPresent(t *testing.T) {
	var i byte
	for ; i < math.MaxInt8; i++ {
		if i < 128 {
			present := isPacketIDPresent(i)
			if present {
				t.Fail()
			}
		} else {
			present := isPacketIDPresent(i)
			if !present {
				t.Fail()
			}
		}
	}
}

func TestIsAscIDPresent(t *testing.T) {
	var i byte
	for ; i < math.MaxInt8; i++ {
		if i < 64 {
			present := isAscIDPresent(i)
			if present {
				t.Fail()
			}
		} else {
			present := isAscIDPresent(i)
			if !present {
				t.Fail()
			}
		}
	}
}

func Test_getSequenceFlag(t *testing.T) {
	type args struct {
		flag byte
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{name: "true", args: args{flag: 32}, want: true},
		{name: "false", args: args{flag: 0}, want: false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getSequenceFlag(tt.args.flag); got != tt.want {
				t.Errorf("getSequenceFlag() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_setSequenceFlag(t *testing.T) {
	type args struct {
		flag         byte
		sequenceFlag bool
	}
	tests := []struct {
		name string
		args args
		want byte
	}{
		{name: "setTrue", args: args{flag: 0, sequenceFlag: true}, want: 32},
		{name: "setFalse", args: args{flag: 0, sequenceFlag: false}, want: 0},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := setSequenceFlag(tt.args.flag, tt.args.sequenceFlag); got != tt.want {
				t.Errorf("setSequenceFlag() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_getAscIDCount(t *testing.T) {
	type args struct {
		flag byte
	}
	tests := []struct {
		name string
		args args
		want byte
	}{
		{name: "get_10", args: args{flag: 10}, want: 10},
		{name: "get_20", args: args{flag: 20}, want: 20},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := getAscIDCount(tt.args.flag); got != tt.want {
				t.Errorf("getAscIDCount() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_setAscIDCount(t *testing.T) {
	type args struct {
		flag  byte
		count byte
	}
	tests := []struct {
		name string
		args args
		want byte
	}{
		{name: "set_10", args: args{flag: 0, count: 10}, want: 10},
		{name: "set_20", args: args{flag: 0, count: 20}, want: 20},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := setAscIDCount(tt.args.flag, tt.args.count); got != tt.want {
				t.Errorf("setAscIDCount() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_sprintFlag(t *testing.T) {
	type args struct {
		flag byte
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{"default", args{flag: 0}, "PacketIDPresent: false, AscIDPresent: false, AscIDCount: 0"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := sprintFlag(tt.args.flag); got != tt.want {
				t.Errorf("sprintFlag() = %v, want %v", got, tt.want)
			}
		})
	}
}
