package delivery

const (
	DataLengthError ManagerError = iota
	PacketLengthError
	SendError
	RejectPacketError
)

type ManagerError byte

type defaultManagerError struct {
	code ManagerError
	msg  string
}

func (e defaultManagerError) Error() string {
	return e.msg
}

func NewManagerError(code ManagerError, reason string) defaultManagerError {
	return defaultManagerError{
		code: code,
		msg:  reason,
	}
}
