package delivery

import "time"

type transmissionData struct {
	Data           []byte
	timeout        time.Duration
	successHandler func()
	failHandler    func(error)
}

type flyingPacket struct {
	id   byte
	data []*transmissionData
	time time.Time
}
