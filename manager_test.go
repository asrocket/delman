package delivery

import (
	"testing"
	"time"
)

func TestNewManager(t *testing.T) {
	manager, err := NewManager(time.Second, nil, 0, false)
	if err == nil {
		t.Errorf("Wrong packet length check")
	}
	manager, err = NewManager(time.Second, nil, 1000, false)
	if err != nil || manager == nil {
		t.Errorf("Error manager creating")
	}
	defer manager.Close()
}

func TestManager_checkPacketID(t *testing.T) {
	manager := Manager{ascIDsChan: make(chan byte, 1)}

	flag := true
	var i byte
	for i = 0; i < 5; i++ {
		flag = !flag
		for j := 0; j < 256; j += 1 + int(i) {
			result := manager.checkPacketID(byte(j), flag)
			id := <-manager.ascIDsChan
			if !result || id != byte(j) {
				t.Errorf("Exp: %d, Act: %d", j, id)
			}
		}
	}

}

func TestManager_getSequenceID(t *testing.T) {
	manager := Manager{}

	flag := true
	var i byte
	for i = 0; i < 5; i++ {
		flag = !flag
		for j := 0; j < 256; j++ {
			id, seqFlag := manager.getSequenceIDAndFlag()
			if id != byte(j) || flag != seqFlag {
				t.Errorf("Exp: %d, Act: %d", j, id)
			}
		}
	}

}

func TestManager_writeAscIDs(t *testing.T) {
	manager := Manager{ascIDsChan: make(chan byte, 10)}

	for i := 0; i < 10; i++ {
		manager.ascIDsChan <- byte(i)
	}

	var header []byte
	result := manager.writeAscIDs(header, 10)

	if len(result) != 10 {
		t.Fail()
		for i := 0; i < 10; i++ {
			if result[i] != byte(i) {
				t.Fail()
			}
		}
	}
}
