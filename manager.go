package delivery

import (
	"fmt"
	"log"
	"time"
)

const reservedByteCount int = 50

//The function will return a ready-to-use delivery manager.
func NewManager(timeout time.Duration, ch chan<- []byte, packetLength int, debug bool) (*Manager, error) {
	if packetLength < reservedByteCount {
		return nil, NewManagerError(PacketLengthError, fmt.Sprintf("Packet length less then reserved byte count: %d", reservedByteCount))
	}
	m := Manager{
		maxPacketLength:            packetLength,
		outChan:                    ch,
		timeout:                    timeout,
		debug:                      debug,
		flyingPackets:              make(map[byte]flyingPacket),
		mapDelChan:                 make(chan byte, 100),
		mapAddChan:                 make(chan flyingPacket, 100),
		packetIDSequence:           0,
		packetSequenceFlag:         false,
		expectedPacketID:           0,
		expectedPacketSequenceFlag: false,
		writerChan:                 make(chan []byte, 10),
		transmissionDataChan:       make(chan transmissionData, 100),
		ascIDsChan:                 make(chan byte, 100),
		closeChan:                  make(chan struct{}),
	}
	go m.run()
	return &m, nil
}

type Manager struct {
	maxPacketLength int
	outChan         chan<- []byte
	timeout         time.Duration
	debug           bool

	flyingPackets map[byte]flyingPacket
	mapDelChan    chan byte
	mapAddChan    chan flyingPacket

	packetIDSequence           byte
	packetSequenceFlag         bool
	expectedPacketID           byte
	expectedPacketSequenceFlag bool

	writerChan           chan []byte
	transmissionDataChan chan transmissionData
	ascIDsChan           chan byte
	closeChan            chan struct{}
}

func (m *Manager) Read(b []byte) (count int, err error) {
	if len(b) <= 0 {
		return 0, nil
	}
	m.debugLog(fmt.Sprintf("Rec Packet Header: [%s] %v", sprintFlag(b[0]), b))
	flag := b[0]
	var confCount byte

	if isPacketIDPresent(flag) {
		packetID := b[1]
		m.debugLog(fmt.Sprintf("Rec packet with id: %d", packetID))
		sequenceFlag := getSequenceFlag(flag)
		if m.checkPacketID(packetID, sequenceFlag) {
			if isAscIDPresent(flag) {
				confCount = getAscIDCount(flag)
				m.confirmPackets(b[2:], confCount)
				return int(2 + confCount), nil
			}
			return 2, nil
		}
		return 2, NewManagerError(RejectPacketError, "Old packet")
	}
	if isAscIDPresent(flag) {
		confCount = getAscIDCount(flag)
		m.confirmPackets(b[1:], confCount)
		return int(1 + confCount), nil
	}
	return 1, nil
}

func (m *Manager) confirmPackets(b []byte, count byte) {
	var i byte
	for ; i < count; i++ {
		ascID := b[i]
		flyingPacket := m.flyingPackets[ascID]
		for _, td := range flyingPacket.data {
			td.successHandler()
			m.mapDelChan <- ascID
		}
	}
}

func (m *Manager) checkPacketID(packetID byte, sequenceFlag bool) bool {
	if packetID >= m.expectedPacketID && sequenceFlag == m.expectedPacketSequenceFlag {
		m.ascIDsChan <- packetID
		m.expectedPacketSequenceFlag = sequenceFlag
		isIncrementSequence := false
		if packetID < 255 {
			m.expectedPacketID = packetID + 1
			isIncrementSequence = true
		}

		if m.expectedPacketID == 255 && !isIncrementSequence {
			m.expectedPacketID = 0
			m.expectedPacketSequenceFlag = !m.expectedPacketSequenceFlag
		}
		return true
	}

	if sequenceFlag != m.expectedPacketSequenceFlag {
		m.ascIDsChan <- packetID
		m.expectedPacketSequenceFlag = sequenceFlag
		m.expectedPacketID = packetID + 1
		return true
	}

	return false
}

func (m *Manager) Write(b []byte) (count int, err error) {
	if len(b) > m.maxPacketLength-reservedByteCount {
		return 0, NewManagerError(DataLengthError, "Data to long")
	}
	doneChan := make(chan error, 1)
	m.transmissionDataChan <- transmissionData{
		Data:    b,
		timeout: m.timeout,
		successHandler: func() {
			doneChan <- nil
		},
		failHandler: func(err error) {
			doneChan <- err
		},
	}
	return len(b), <-doneChan
}

func (m *Manager) Close() error {
	m.closeChan <- struct{}{}
	return nil
}

func (m *Manager) handlePacketTimeout() {
	for key, value := range m.flyingPackets {
		if value.time.Add(m.timeout).Before(time.Now()) {
			go func(packet flyingPacket) {
				for _, td := range packet.data {
					td.failHandler(NewManagerError(SendError, "Manager: Packet timeout"))
				}
			}(value)
			m.mapDelChan <- key
		}
	}
}

func (m *Manager) writeFlyingPacket(data []byte, td []*transmissionData, timeout time.Duration) {
	header, id := m.getPacketHeaderAndId(len(td) > 0)
	result := append(header, data...)
	m.debugLog(fmt.Sprintf("Packet Header: [%s] %v", sprintFlag(header[0]), header))
	m.debugLog(fmt.Sprintf("Write flying packet: %d. Trannsmission data count: %d", id, len(td)))
	m.mapAddChan <- flyingPacket{id: id, data: td, time: time.Now()}
	m.writerChan <- result
}

func (m *Manager) getPacketHeaderAndId(writePacketID bool) (header []byte, packetID byte) {
	writeAscID := false
	var ascIDCount byte
	if len(m.ascIDsChan) > 0 {
		writeAscID = true
		if len(m.ascIDsChan) > 31 {
			ascIDCount = 31
		} else {
			ascIDCount = byte(len(m.ascIDsChan))
		}
	}

	var flag byte
	if writePacketID {
		flag = setPacketIDPresent(flag)
	}

	if writeAscID {
		flag = setAscIDPresent(flag)
		flag = setAscIDCount(flag, ascIDCount)
	}

	header = append(header, flag)
	if writePacketID {
		id, seqFlag := m.getSequenceIDAndFlag()
		packetID = id
		flag = setSequenceFlag(flag, seqFlag)
		header = append(header, id)
	}

	if writeAscID {
		header = m.writeAscIDs(header, ascIDCount)
	}

	return
}

func (m *Manager) getSequenceIDAndFlag() (id byte, flag bool) {
	id = m.packetIDSequence
	flag = m.packetSequenceFlag

	isIncrementSequence := false
	if m.packetIDSequence < 255 {
		m.packetIDSequence++
		isIncrementSequence = true
	}
	if m.packetIDSequence == 255 && !isIncrementSequence {
		m.packetIDSequence = 0
		m.packetSequenceFlag = !m.packetSequenceFlag
	}
	return
}

func (m *Manager) writeAscIDs(header []byte, ascIDCount byte) []byte {
	var i byte
	for ; i < ascIDCount; i++ {
		id := <-m.ascIDsChan
		header = append(header, id)
	}
	return header
}

func (m *Manager) buildAndWriteFlyingPacket() {
	packetCount := len(m.transmissionDataChan)
	if packetCount == 0 {
		return
	}

	td := make([]*transmissionData, 0)
	byteArr := make([]byte, 0)
	timeout := 24 * time.Hour

	for i := 0; i < packetCount; i++ {
		if len(m.transmissionDataChan) == 0 {
			break
		}

		transmissionData := <-m.transmissionDataChan
		if len(transmissionData.Data) > m.maxPacketLength {
			break
		}
		if len(byteArr)+len(transmissionData.Data) > m.maxPacketLength {
			break
		}
		if transmissionData.timeout < timeout {
			timeout = transmissionData.timeout
		}
		byteArr = append(byteArr, transmissionData.Data...)
		td = append(td, &transmissionData)
	}
	if len(td) > 0 {
		m.writeFlyingPacket(byteArr, td, timeout)
	}
}

func (m *Manager) debugLog(v string) {
	if !m.debug {
		return
	}
	log.Println("Manager:", v)
}

func (m *Manager) run() {
	ticker := time.Tick(time.Millisecond)
	for {
		select {
		case data := <-m.writerChan:
			{
				m.debugLog(fmt.Sprintf("Write data in writer: %v", data))
				m.outChan <- data
			}
		case packet := <-m.mapAddChan:
			{
				m.flyingPackets[packet.id] = packet
			}
		case key := <-m.mapDelChan:
			{
				delete(m.flyingPackets, key)
			}
		case <-ticker:
			{
				sendEmptyPacket := len(m.ascIDsChan) > 0 && len(m.transmissionDataChan) == 0
				if len(m.transmissionDataChan) > 0 {
					m.buildAndWriteFlyingPacket()
				}
				if sendEmptyPacket {
					m.writeFlyingPacket(make([]byte, 0), make([]*transmissionData, 0), 0)
				}
				m.handlePacketTimeout()
			}

		case <-m.closeChan:
			{
				close(m.closeChan)
				close(m.transmissionDataChan)
				close(m.writerChan)
				close(m.mapAddChan)
				close(m.mapDelChan)
				close(m.ascIDsChan)
				return
			}
		}
	}
}
